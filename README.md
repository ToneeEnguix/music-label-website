// DESCRIPTION //

This repo was made as part of the Barcelona Code School Full-Stack bootcamp.

It is a website for a local music-label using only HTML and CSS.

It is fully working with actual content and links to their other online sites like Discogs.

I found it very useful for learning to build websites from sctrach.


// LIVE DEMO //

www.ultralocal.surge.sh/